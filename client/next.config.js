/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  env: {
    API: process.env.API,
  },
  images: {
    domains: ["ukrnstand.fra1.digitaloceanspaces.com"],
  },
};

module.exports = nextConfig;
