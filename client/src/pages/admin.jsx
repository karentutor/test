import { Fragment, useEffect, useState } from "react";
import axios from "axios";
import AdminRegisterForm from "../components/forms/AdminRegisterForm";
import AddStoryForm from "../components/forms/AddStoryForm";
import useSidebar from "../hooks/useSidebar";
import SidebarButton from "../components/SidebarButton";
import styles from "../styles/pages/Admin.module.scss";
import { signout } from "../helpers/helpers";
// redux
import { useSelector, useDispatch } from "react-redux";
import { logout } from "../redux/features/auth";

//export default function Admin({ authorized, user }) {
export default function Admin() {
  const [sidebar, toggleSidebar] = useSidebar(true);

  const dispatch = useDispatch();

  let user = "";

  const auth = useSelector((state) => state.auth.value);
  const handleActive = (target) => {
    setActive(target);
    toggleSidebar();
  };

  const handleLogout = () => {
    signout();
    dispatch(logout());
    // navigate("/");
  };

  return (
    <>
      <div className={styles.container}>
        <nav className={sidebar ? styles.active : ""}>
          <div>
            <button type="button" onClick={() => handleActive("content")}>
              Add content
            </button>
            <button type="button" onClick={() => handleActive("admin")}>
              Add new admin
            </button>
            <button type="button" onClick={() => handleActive("invite")}>
              Add new user
            </button>
          </div>
          <div>
            {sidebar && (
              <button className={styles.hideButton} onClick={toggleSidebar}>
                X
              </button>
            )}
            <button
              type="button"
              className={styles.logoutButton}
              onClick={handleLogout}
            >
              Log out
            </button>
          </div>
        </nav>
        {!sidebar && <SidebarButton toggleSidebar={toggleSidebar} tablet />}
        <div className={styles.right}>
          <h1>Welcome, {user}</h1>

          {active == "content" && (
            <div>
              <p>Add new content for the landing page</p>
              <AddStoryForm />
            </div>
          )}

          {active == "admin" && (
            <div>
              <p>Signup as new admin</p>
              <AdminRegisterForm />
            </div>
          )}
        </div>
      </div>
    </>
  );
}

export async function getServerSideProps() {
  return {
    props: {
      authorized: true, // should be the result of authentication logic
      user: "Hannah",
    },
  };
}
