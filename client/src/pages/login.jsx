import { useState } from "react";
import axios from "axios";
import PasswordInput from "../components/PasswordInput";
import { authenticate } from "../helpers/helpers";
import { toast } from "react-toastify";
import styles from "../styles/pages/LoginSignup.module.scss";
import atoms from "../styles/atoms.module.scss";
import { useRouter } from "next/router";
//redux
import { useDispatch, useSelector } from "react-redux";
import { signin } from "../redux/features/auth";

export default function Login() {
  const [email, setEmail] = useState("test@test.com");
  const [password, setPassword] = useState("12345678");

  // router
  const router = useRouter();
  // redux
  const auth = useSelector((state) => state.auth.value);
  const dispatch = useDispatch();

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const { data } = await axios.post(`api/login`, {
        email,
        password,
      });
      // set cookie and localstorage
      if (data) {
        // set cookie and localstorage
        authenticate(data);
        // // add to state
        dispatch(signin(data));
        router.push("/");
        // console.log(data.user.role);
        // if (res.data.user.role === "learner") navigate("/dashboard");
        // if (res.data.user.role === "teacher") navigate("/dashboard/seller");
      }
    } catch (err) {
      console.log("fail");
      // toast(err.response.data);
    }
  };

  return (
    <div className={styles.container}>
      <div className={styles.formWrapper}>
        <h1>Log in</h1>
        <form
          className={atoms.form}
          style={{ justifyContent: "space-between", flexGrow: 1 }}
          onSubmit={handleSubmit}
        >
          <div className={styles.inputsWrapper}>
            <input
              className={atoms.input}
              type="email"
              name="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              placeholder="Email"
              required
            />
            <PasswordInput
              minLength={8}
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <button
            className={`${atoms.button} ${styles.loginButton}`}
            type="submit"
          >
            Log in
          </button>
        </form>
      </div>
    </div>
  );
}
