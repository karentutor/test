import { Fragment } from "react";
import { PersistGate } from 'redux-persist/integration/react'
import "bootstrap/dist/css/bootstrap.min.css";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Provider } from "react-redux";
import { persistor, store } from "../redux/store";
import Header from "../components/Header";
import "../styles/globals.scss";
// import { Provider } from "../context";

function MyApp({ Component, pageProps }) {
  return (
    <Provider store={store}>
      <PersistGate loading={<div>loading...</div>} persistor={persistor}>
        <ToastContainer position="top-center" />
        <Header />
        <Component {...pageProps} />
      </PersistGate>
    </Provider>
  );
}

export default MyApp;
