import { useEffect, useRef, useState } from "react";
import useSidebar from "../hooks/useSidebar";
import Carousel from "../components/Carousel";
import SidebarButton from "../components/SidebarButton";
import axios from "axios";
import { toast } from "react-toastify";
import styles from "../styles/pages/Home.module.scss";
import { handleDate } from "../helpers/helpers";

export default function Home({ data }) {
  const [active, toggleSidebar] = useSidebar(true);
  const [stories, setStories] = useState([]);

  const [index, setIndex] = useState(0);

  // num = (num + 1) % 4;

  useEffect(() => {
    getStories();
  }, []);

  const getStories = async () => {
    const { data } = await axios.get(`/api/stories`);
    if (data) setStories(data.filter(item => item.image?.Location));
  };

  useEffect(() => {
    if (stories.length > 0) {
      const timer = setInterval(() => {
        setIndex((current) => (current == stories.length - 1 ? 0 : current + 1)); // <-- Change this line!
        // setIndex((current) => (current == stories.length ? 0 : current + 1)); // <-- Change this line!
      }, 5000);
      return () => {
        clearInterval(timer);
      };
    }
  }, [stories]);

  return (
    <>
      <section className={styles.hero}>
        <div className={`${styles.left} ${active ? styles.show : ""}`}>
          <button onClick={(e) => handleStoryType(e, "in-memory")}>
            In Memory
          </button>
          <button onClick={(e) => handleStoryType(e, "missing")}>
            Missing
          </button>
          {active && (
            <button onClick={toggleSidebar} className={styles.hideButton}>
              X
            </button>
          )}
        </div>
        {!active && <SidebarButton toggleSidebar={toggleSidebar} />}
        <div className={styles.right}>
          {stories[index] && <Carousel image={stories[index].image.Location} alt="an image" />}
        </div>
      </section>
      <section className={styles.description}>
        <div className={styles.name}>
          <span>content</span>
        </div>
        content
        <div className={styles.story}>
          <p>content</p>
        </div>
      </section>
    </>
  );
}

// export async function getStaticProps() {
//   return {
//     props: {
//       // name: "John Brown",
//       // dob: "06-16-1987",
//       // story:
//       //   "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque cursus odio nibh, et vehicula neque sollicitudin vel. Aliquam erat volutpat. Sed efficitur libero vitae ante fermentum ultricies. Donec fringilla diam ac magna viverra placerat. Duis vel magna dui. Suspendisse a sodales ligula. Vestibulum justo risus, feugiat et nulla nec, varius vestibulum nisl. Sed efficitur libero vitae ante fermentum ultricies. Donec fringilla diam ac magna viverra placerat.",
//       data: [
//         {
//           name: "John Brown",
//           dob: "06-16-1987",
//           story:
//             "This is the first element, consectetur adipiscing elit. Quisque cursus odio nibh, et vehicula neque sollicitudin vel. Aliquam erat volutpat. Sed efficitur libero vitae ante fermentum ultricies. Donec fringilla diam ac magna viverra placerat. Duis vel magna dui. Suspendisse a sodales ligula. Vestibulum justo risus, feugiat et nulla nec, varius vestibulum nisl. Sed efficitur libero vitae ante fermentum ultricies. Donec fringilla diam ac magna viverra placerat.",
//           image: "/family.jpg",
//           alt: "family of four walking in the street",
//         },
//         {
//           name: "Louise Smith",
//           dob: "11-22-1990",
//           story:
//             "This is the second element, consectetur adipiscing elit. Quisque cursus odio nibh, et vehicula neque sollicitudin vel. Aliquam erat volutpat. Sed efficitur libero vitae ante fermentum ultricies. Duis vel magna dui. Suspendisse a sodales ligula. Vestibulum justo risus, feugiat et nulla nec, varius vestibulum nisl. Sed efficitur libero vitae ante fermentum ultricies. Donec fringilla diam ac magna viverra placerat. Donec fringilla diam ac magna viverra placerat.",
//           image: "/woman.jpg",
//           alt: "woman wearing a yellow shirt with long sleeves",
//         },
//         {
//           name: "Sabo",
//           dob: "02-27-2016",
//           story:
//             "Here is the thrid element, consectetur adipiscing elit. Quisque cursus odio nibh, et vehicula neque sollicitudin vel. Sed efficitur libero vitae ante fermentum ultricies. Donec fringilla diam ac magna viverra placerat. Aliquam erat volutpat. Duis vel magna dui. Suspendisse a sodales ligula. Vestibulum justo risus, feugiat et nulla nec, varius vestibulum nisl. Sed efficitur libero vitae ante fermentum ultricies. Donec fringilla diam ac magna viverra placerat.",
//           image: "/dog.jpg",
//           alt: "dog with golden fur",
//         },
//       ],
//     },
//   };
// }
