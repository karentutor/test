import { useEffect, useState } from "react";
import axios from "axios";
import { useRouter } from "next/router";
import { SyncOutlined } from "@ant-design/icons";
import InstructorNav from "../nav/InstructorNav";

// if current instructor -- we allow them to access the page
const InstructorRoute = ({ children }) => {
  // state
  const [ok, setOk] = useState(false);
  // router
  const router = useRouter();

  useEffect(() => {
    getAdmin();
  }, []);

  const getAdmin = async () => {
    try {
      const { data } = await axios.get(
        "http://localhost:8000/api/current-user"
      );
      console.log("INSTRUCTOR ROUTE => ", data);
      if (data.ok) setOk(true);
    } catch (err) {
      console.log(err);
      setOk(false);
      router.push("/");
    }
  };

  return <>{!ok ? <div>Waiting or not authorized</div> : <>{children}</>}</>;
};

export default InstructorRoute;
