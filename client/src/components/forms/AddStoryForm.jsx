import { useState } from "react";
import axios from "axios";
import atoms from "../../styles/atoms.module.scss";
import forms from "../../styles/components/forms.module.scss";
import { useRouter } from "next/router";
import { toast } from "react-toastify";
import Resizer from "react-image-file-resizer";

let defaultValues = {
  firstName: "",
  lastName: "",
  dob: "",
  storyType: false,
  storyStatus: true,
  story: "",
};

export default function AddStoryForm() {
  const [values, setValues] = useState({
    firstName: "",
    lastName: "",
    dob: "",
    storyType: false,
    storyStatus: true,
    story: "",
  });

  const [image, setImage] = useState({});
  const router = useRouter();

  const [uploadButtonText, setUploadButtonText] = useState("Upload Image");

  const handleImage = (e) => {
    let file = e.target.files[0];
    // setPreview(window.URL.createObjectURL(file));
    setUploadButtonText(file.name);
    // setValues({ ...values, loading: true });
    // resize
    Resizer.imageFileResizer(file, 720, 500, "JPEG", 100, 0, async (uri) => {
      try {
        let { data } = await axios.post("/api/story/upload-image", {
          image: uri,
        });
        // console.log("IMAGE UPLOADED", data);
        // set image in the state
        setImage(data);
        // setValues(defaultValues);
      } catch (err) {
        console.log(err);
        // setValues({ ...values, loading: false });
        toast("Image upload failed. Try later.");
      }
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    console.table();
    try {
      // console.log(values);
      const { data } = await axios.post(`/api/story`, {
        ...values, //spread values
        image,
      });
      toast("Story submitted!");
      router.push("/admin");
    } catch (err) {
      toast(err.response.data);
    }
  };

  return (
    <form className={`${atoms.form} ${forms.addContentForm}`}>
      <div>
        <input
          className={atoms.input}
          type="text"
          name="first-name"
          value={values.firstName}
          onChange={(e) => setValues({ ...values, firstName: e.target.value })}
          placeholder="First name"
          required
        />
        <input
          className={atoms.input}
          type="test"
          name="last-name"
          value={values.lastName}
          onChange={(e) => setValues({ ...values, lastName: e.target.value })}
          placeholder="Last name"
          required
        />
      </div>
      <label htmlFor="date" className={atoms.label}>
        Date of birth
      </label>
      <input
        className={atoms.input}
        type="date"
        name="date-of-birth"
        value={values.dob}
        onChange={(e) => setValues({ ...values, dob: e.target.value })}
        required
      />
      <select
        className={atoms.input}
        name="story-type"
        value={values.storyType}
        onChange={(e) => setValues({ ...values, storyType: e.target.value })}
        required
      >
        <option>-- Select a type --</option>
        <option value="person">Person</option>
        <option value="animal">Animal</option>
      </select>
      <select
        className={atoms.input}
        name="story-status"
        value={values.storyStatus}
        onChange={(e) => setValues({ ...values, storyStatus: e.target.value })}
        required
      >
        <option>-- Select a status --</option>
        <option value="in-memory">In Memory</option>
        <option value="missing">Missing</option>
        <option value="heroes">Heroes</option>
        <option value="our-lives-now">Our lives now</option>
      </select>
      <label
        style={{
          backgroundColor: "transparent",
          color: "black",
          border: "1px solid black",
        }}
        className={atoms.button}
      >
        {uploadButtonText}
        <input
          type="file"
          name="image"
          onChange={handleImage}
          accept="image/*"
          hidden
        />
      </label>

      <textarea
        className={atoms.textarea}
        name="story"
        value={values.story}
        onChange={(e) => setValues({ ...values, story: e.target.value })}
        placeholder="Story..."
        rows={5}
        required
      />
      <button className={atoms.button} type="submit" onClick={handleSubmit}>
        Add
      </button>
    </form>
  );
}
