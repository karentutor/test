import { useState } from "react";
import axios from "axios";
import { useRouter } from "next/router";
import { toast } from "react-toastify";
import PasswordInput from "../PasswordInput";
import atoms from "../../styles/atoms.module.scss";

export default function AdminRegisterForm() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  // router
  const router = useRouter();

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const { data } = await axios.post(
        // `${process.env.NEXT_PUBLIC_API}/register`,
        "http://localhost:8000/api/register",
        {
          name,
          email,
          password,
        }
      );
      // console.log("REGISTER RESPONSE", data);
      toast("Registration successful. Please login.");
      router.push("/login");
      // setLoading(false);
    } catch (err) {
      toast(err.response.data);
      // setLoading(false);
    }
  };

  return (
    <form className={atoms.form} onSubmit={handleSubmit}>
      <input
        className={atoms.input}
        type="text"
        name="name"
        value={name}
        onChange={(e) => setName(e.target.value)}
        placeholder="Name"
        required
      />

      <input
        className={atoms.input}
        type="email"
        name="email"
        value={email}
        onChange={(e) => setEmail(e.target.value)}
        placeholder="Email"
        required
      />
      <PasswordInput
        minLength={8}
        value={password}
        onChange={(e) => setPassword(e.target.value)}
      />
      <button className={atoms.button} type="submit">
        Register
      </button>
    </form>
  );
}
