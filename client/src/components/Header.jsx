import Link from "next/link";
import { useEffect, useState } from "react";
import styles from "../styles/components/Header.module.scss";
import { signout } from "../helpers/helpers";
import { useRouter } from "next/router";
// redux
import { useSelector, useDispatch } from "react-redux";
import { logout } from "../redux/features/auth";

export default function Header() {
  const [showNav, setShowNav] = useState(false);
  const [isAdmin, setIsAdmin] = useState(false);

  const auth = useSelector((state) => state.auth.value);
  const dispatch = useDispatch();
  const router = useRouter();

  useEffect(() => {
    auth && setIsAdmin(auth.user.role.includes("admin") ? true : false);
    !auth && setIsAdmin(false);
  }, [auth]);

  const toggleNav = () => {
    setShowNav((current) => !current);
  };

  const handleLogout = () => {
    signout();
    dispatch(logout());
    router.push("/");
  };

  return (
    <>
      <header className={styles.container}>
        <nav className={`${styles.nav} ${showNav ? styles.show : ""}`}>
          <div className={styles.navLeft}>
            <Link href="/">
              <a>Home</a>
            </Link>
            <Link href="/about-us">
              <a>About us</a>
            </Link>
            <Link href="/contact-us">
              <a>Contact us</a>
            </Link>
            {isAdmin && (
              <Link href="/admin">
                <a
                  className={styles.login}
                  style={{ background: "transparent", color: "red" }}
                >
                  admin
                </a>
              </Link>
            )}
          </div>

          {auth ? (
            <span
              className={styles.login}
              // style={ cursor: "pointer" }
              // style={{ cursor: "pointer" }}
              onClick={handleLogout}
            >
              Logout
            </span>
          ) : (
            <Link href="/login">
              <a className={styles.login}>Login</a>
            </Link>
          )}
        </nav>
        <button
          aria-label="Menu button"
          className={`${styles.hamburger} ${showNav ? styles.active : ""}`}
          onClick={toggleNav}
        >
          <div aria-hidden />
          <div aria-hidden />
          <div aria-hidden />
        </button>
      </header>
    </>
  );
}
