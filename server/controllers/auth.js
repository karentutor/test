import User from "../models/user";
import { hashPassword, comparePassword } from "../utils/auth";
import jwt from "jsonwebtoken";
import nanoid from "nanoid";
// sendgrid

export const login = async (req, res) => {
  try {
    console.log(req.body);
    const { email, password } = req.body;
    // check if our db has user with that email
    const user = await User.findOne({ email }).exec();
    console.log(user);
    if (!user) return res.status(400).send("No user found");
    // check password
    const match = await comparePassword(password, user.password);
    if (!match) return res.status(400).send("Wrong password");
    // create signed jwt
    //when expire --> execute the logout
    // make request to b/e clear token from the cookie
    // empty the user from the context f/e and from local storage
    // all this when expire
    // easiest and most effective way is configuring axios -> interceptor
    // look for res. 400 -> means not authorized
    // here we do in the f/e

    const token = jwt.sign({ _id: user._id }, process.env.JWT_SECRET, {
      expiresIn: "7d",
    });
    // return user and token to client, exclude hashed password
    user.password = undefined;
    // send token in cookie
    res.cookie("token", token, {
      httpOnly: true,
      // secure: true, // only works on https
    });
    // send user as json response
    res.json({ token, user });
  } catch (err) {
    console.log(err);
    return res.status(400).send("Error. Try again.");
  }
};

export const register = async (req, res) => {
  try {
    // console.log(req.body);
    const { name, email, password } = req.body;
    // validation
    if (!name) return res.status(400).send("Name is required");
    if (!password || password.length < 8) {
      return res
        .status(400)
        .send("Password is required and should be min 8 characters long");
    }
    let userExist = await User.findOne({ email }).exec();
    if (userExist) return res.status(400).send("Email is taken");

    // hash password
    const hashedPassword = await hashPassword(password);

    register;
    const user = new User({
      name,
      email,
      password: hashedPassword,
    });
    await user.save();
    // console.log("saved user", user);
    // don't really need the user -- just a success response
    return res.json({ ok: true });
  } catch (err) {
    console.log(err);
    //   return res.status(400).send("Error. Try again.");
  }
};
