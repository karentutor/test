import Story from "../models/story";
import AWS from "aws-sdk";
import nanoid from "nanoid";

export const getStories = async (req, res) => {
  const all = await Story.find().limit(50).exec();
  res.json(all);
};
