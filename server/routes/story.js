import express from "express";

const router = express.Router();

// middleware
import { adminRoute, requireSignin } from "../middleware";

import { getStories } from "../controllers/story";
router.get("/stories", getStories);

module.exports = router;
