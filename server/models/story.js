const mongoose = require("mongoose");
const { ObjectId } = mongoose.Schema;

const storySchema = new mongoose.Schema(
  {
    test: {
      type: String,
      trim: true,
      required: true,
    },
    postedBy: { type: ObjectId, ref: "User" },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Story", storySchema);
